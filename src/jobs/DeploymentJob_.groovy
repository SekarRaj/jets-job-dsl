def createPipelineJob() {
    return pipelineJob('Deployment') {
        def repository = 'https://gitlab.com/SekarRaj/deployment.git'

        description 'Build and test the service'

        triggers {
            scm('* * * * *')
        }

        definition {
            cpsScm {
                scm {
                    git {
                        remote {
                            url(repository)
                            credentials('gitlab-creds')
                        }
                        branches('master')
                        scriptPath('Jenkinsfile')
                        extensions {} //Required to prevent Git tag
                    }
                }
            }
        }
    }
}

createPipelineJob()
