def createPipelineJob() {
    return pipelineJob('Product-Service') {
        def repository = 'https://gitlab.com/SekarRaj/product-service.git'

        description 'Build and test the service'

        triggers {
            scm('* * * * *')
        }

        definition {
            cpsScm {
                scm {
                    git {
                        remote {
                            url(repository)
                            credentials('gitlab-creds')
                        }
                        branches('master', 'development')
                        scriptPath('Jenkinsfile')
                        extensions {} //Required to prevent Git tag
                    }
                }
            }
        }
    }
}

createPipelineJob()
