def createPipelineJob(def jobName, def repoName) {
    return pipelineJob(jobName) {
        def repository = "https://gitlab.com/SekarRaj/${repoName}.git"

        description 'CI/CD Pipeline'

        triggers {
            scm('* * * * *')
        }

        definition {
            cpsScm {
                scm {
                    git {
                        remote {
                            url(repository)
                            credentials('gitlab-creds')
                        }
                        branches('master', 'development')
                        scriptPath('Jenkinsfile')
                        extensions {} //Required to prevent Git tag
                    }
                }
            }
        }
    }
}

createPipelineJob('Product-Service','product-service')
createPipelineJob('Deployment','deployment') 
