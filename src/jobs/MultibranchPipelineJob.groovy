return multibranchPipelineJob('Product-MultiBranch') {
    description('Sample set up for creating multi branch pipeline')
    
    branchSources {
        git {
            remote('https://gitlab.com/SekarRaj/product-service.git')
            credentialsId('gitlab-creds')
        }
    }
    orphanedItemStrategy {
        discardOldItems {
            numToKeep(20)
        }
    }
}